jQuery(document).ready(function() {
    
    // simple boxes
    jQuery(".sm-asb").each(function(){
       var animation = jQuery(this).attr("data-animation");
       jQuery(this).hover(
        function() { 
            jQuery(this).find(".sm-etsb-icon img,.sm-fasb-icon i,.sm-fasb-icon img").addClass("animated " + animation);
        },
        function() { 
            jQuery(this).find(".sm-etsb-icon img,.sm-fasb-icon i,.sm-fasb-icon img").removeClass("animated " + animation);
        }
       ); 
    });
    
    
    // flipped card    
    jQuery( ".sm-asb-card .sm-asb:nth-child(1)" ).addClass( "sm-asb-front" );
    jQuery( ".sm-asb-card .sm-asb:nth-child(2)" ).addClass( "sm-asb-back" );
    
    jQuery(".sm-asb-card").each(function(){
       var animation = jQuery(this).attr("data-animation");      
       jQuery(this).hover(
        function() { 
            jQuery(this).addClass("sm-asb-flipped");
        },
        function() { 
            jQuery(this).removeClass("sm-asb-flipped");
        }
       ); 
    });    

});
