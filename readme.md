Shortcodes Master - Animated service boxes
==================

Installation
------------

You are installing this script as any other WordPress plugin. The only requirement is installed Shortcodes Master plugin. Lizatom shortcodes plugin is its addon.

1. Go to the Plugins -> Add New -> Upload
2. Upload shortcodes-master-animated-service-boxes.zip
3. Activate Shortcodes Master - Animated service boxes in the Plugins page


Getting started
---------------

The Shortcodes Master brings you option to insert your shortcodes easily with the shortcodes generator.

Shortcodes overview
-------------------

### Elegant themes service box

[Demo >](https://lizatom.com/demo/shortcodes-master/animated-service-boxes/#elegant-themes-icons)

Example

``` html
[sm_asb_et id="asb-et-unique-id-1" icon="image: calculator" icon_bgcolor="#ffffff" title="Web dvelopment" title_bgcolor="#342f45" title_color="#ffffff" title_tag="h3" text_align="justify" text_bgcolor="#f0eaea" text_color="#45515e"]Duis id nunc dolor. Duis nec urna pretium, scelerisque ligula at, lacinia libero. Praesent tristique, justo id maximus commodo, purus orci fringilla sapien, ut pulvinar arcu purus eget ex. Nunc nibh enim, placerat quis molestie in, maximus in est. [/sm_asb_et]
```

Please note that you have to enter unique ID of the box.

### Font Awesome service box

[Demo >](https://lizatom.com/demo/shortcodes-master/animated-service-boxes/#font-awesome-icons)

Example

``` html
[sm_asb_fa id="asb-fa-unique-id-1" icon="icon: codepen" animation="wobble" icon_size="134" shadow="0px 0px 0px #AAB2BD" title_bgcolor="#cbc9c9" title_color="#063d75" text_align="justify" text_bgcolor="#f5f5f5" text_color="#000000"]In hac habitasse platea dictumst. In id lacus mi. Phasellus in diam vitae mauris venenatis tristique facilisis nec diam. Sed placerat turpis metus, id cursus urna pharetra sed. Curabitur nisl dui, molestie non arcu id, vulputate volutpat dolor.[/sm_asb_fa]
```

### Flipped card box 

[Demo >](https://lizatom.com/demo/shortcodes-master/animated-service-boxes/#flip-card-boxes)

Example

``` html
[sm_asb_fc min_height="300"]
[sm_asb_et id="asb-et-unique-id-1" icon="image: calculator" animation="animation-off" icon_bgcolor="#ffffff" title="Web development" title_bgcolor="#342f45" title_color="#ffffff" title_tag="h3" text_align="justify" text_bgcolor="#f0eaea" text_color="#45515e"]Duis id nunc dolor. Duis nec urna pretium, scelerisque ligula at, lacinia libero. Praesent tristique, justo id maximus commodo, purus orci fringilla sapien, ut pulvinar arcu purus eget ex. Nunc nibh enim, placerat quis molestie in, maximus in est. [/sm_asb_et]
[sm_asb_et id="asb-et-unique-id-1" icon="image: cmyk" animation="tada" icon_bgcolor="#ffffff" title="Design" title_bgcolor="#342f45" title_color="#ffffff" title_tag="h3" text_align="justify" text_bgcolor="#f0eaea" text_color="#45515e"]Duis id nunc dolor. Duis nec urna pretium, scelerisque ligula at, lacinia libero. Praesent tristique, justo id maximus commodo, purus orci fringilla sapien, ut pulvinar arcu purus eget ex. Nunc nibh enim, placerat quis molestie in, maximus in est. [/sm_asb_et]
[/sm_asb_fc]
```

It's important you to define minimal height of the animated box:

``` html
[sm_asb_fc min_height="300"]
```

You can combine both ElegantThemes' icons and Font Awesome icons.



