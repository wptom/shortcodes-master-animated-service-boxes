<?php
class Shortcodes_Master_Asb {

	/**
	 * Constructor
	 */
	function __construct() {
		// Load textdomain
		load_plugin_textdomain( 'smasb', false, dirname( plugin_basename( SMASB_PLUGIN_FILE ) ) . '/lang/' );
		// Reset cache on activation
		if ( class_exists( 'Sm_Generator' ) ) register_activation_hook( SMASB_PLUGIN_FILE, array( 'Sm_Generator', 'reset' ) );
		// Init plugin
		add_action( 'plugins_loaded', array( __CLASS__, 'init' ), 20 );
		// Make pluin meta translation-ready
		__( 'ShortcodesMaster', 'smasb' );
		__( 'Shortcodes Master - Animated service boxes', 'smasb' );
		__( 'Animated service boxes add-on for Shortcodes Master.', 'smasb' );
	}

	/**
	 * Plugin init
	 */
	public static function init() {
		// Check for SM
		if ( !function_exists( 'shortcodes_master' ) ) {
			// Show notice
			add_action( 'admin_notices', array( __CLASS__, 'sm_notice' ) );
			// Break init
			return;
		}
		// Register assets
		add_action( 'init', array( __CLASS__, 'assets' ) );
		// Add new group to Generator
		add_filter( 'sm/data/groups', array( __CLASS__, 'group' ) );
		// Register new shortcodes
		add_filter( 'sm/data/shortcodes', array( __CLASS__, 'data' ) );
		// Add plugin meta links
		add_filter( 'plugin_row_meta', array( __CLASS__, 'meta_links' ), 10, 2 );
	}

	/**
	 * Install SM notice
	 */
	public static function sm_notice() {
		?><div class="updated">
			<p><?php _e( 'Please install and activate latest version of <b>Shortcodes Master</b> to use it\'s addon <b>Shortcodes Master - Animated service boxes</b>.<br />Deactivate this addon to hide this message.', 'smasb' ); ?></p>
			<p><a href="https://lizatom.com/wordpress-shortcodes-plugin/" target="_blank" class="button button-primary"><?php _e( 'Install Shortcodes Master', 'smasb' ); ?> &rarr;</a></p>	
		</div><?php
	}

	public static function assets() { 
        $plugins_url = plugins_url();

        wp_register_style( 'smasb', plugins_url( 'assets/css/smasb.css', SMASB_PLUGIN_FILE ), false, SMASB_PLUGIN_VERSION, 'all' );
		wp_register_style( 'smhover', plugins_url( 'assets/css/hover-min.css', SMASB_PLUGIN_FILE ), false, SMASB_PLUGIN_VERSION, 'all' );
        
        wp_register_script('jquery', includes_url('js/jquery/jquery.js'));       
        wp_register_script( 'smasb', plugins_url( 'assets/js/smasb.js', SMASB_PLUGIN_FILE ), array('jquery'), SMASB_PLUGIN_VERSION, true );
        
	}

	public static function meta_links( $links, $file ) {
		if ( $file === plugin_basename( SMASB_PLUGIN_FILE ) ) { $links[] = '<a href="https://lizatom.com/wiki/shortcodes-master-animated-boxes/" target="_blank">' . __( 'Help', 'smasb' ) . '</a>';
		}
		return $links;
		
	}

	/**
	 * Add new group to the Generator
	 */
	public static function group( $groups ) {
		//$groups['group'] = __( 'Group', 'smasb' );
		return $groups;
	}

	/**
	 * New shortcodes data
	 */
	public static function data( $shortcodes ) {
        
        $et_icons = array('anchor', 'aperture', 'arrow-down', 'arrow-up', 'art', 'barchart', 'batteryfull', 'batterylow', 'bike', 'biker', 'bikewheel', 'blimp', 'bolt', 'bomb', 'booklet', 'bookshelf', 'briefcase', 'brightness', 'browser', 'brush-pencil', 'calculator', 'calendar', 'camera', 'car', 'cart', 'carwheel', 'caution', 'chat', 'check', 'circlecompass', 'clapboard', 'clipboard', 'clock', 'cloud', 'cmyk', 'colorwheel', 'compass', 'compose', 'computer', 'cone', 'contacts', 'contrast', 'countdown', 'creditcard', 'crop', 'crossroads', 'cruise', 'cursor', 'denied', 'dev', 'die', 'document', 'dolly', 'door', 'download', 'easel', 'email', 'eye', 'eyedropper', 'fashion', 'filmreel', 'filmroll', 'flag', 'flame', 'flash', 'flower', 'focus', 'folder', 'frames', 'gamecontroller', 'gas', 'gear', 'genius', 'global', 'globe', 'gps', 'hazard', 'heart', 'helicopter', 'hotair', 'hourglass', 'image', 'interstate', 'key', 'keyboard', 'lens', 'lightbulb', 'loading', 'location', 'locked', 'magicwand', 'magnifyingglass', 'mail', 'map', 'megaphone', 'megaphone2', 'memorycard', 'merge', 'mic', 'microphone', 'money', 'motorcycle', 'music', 'news', 'paintbrush', 'paintbrush2', 'paintcan', 'paintroller', 'parachute', 'pencil', 'phone', 'piechart', 'pin', 'pin2', 'plane', 'play', 'plugin', 'polaroid', 'polaroidcamera', 'polaroids', 'power', 'present', 'profle', 'quote', 'racingflags', 'radio', 'radiotower', 'rainbow', 'recycle', 'rgb', 'ribbon', 'roadblock', 'rocket', 'rulertriangle', 'running', 'sailboat', 'schooolbus', 'scissors', 'scooter', 'security', 'selftimer', 'settings', 'shipwheel', 'shoeprints', 'shop', 'skateboard', 'slr', 'smartphone', 'spaceshuttle', 'speaker', 'speedometer', 'spraypaint', 'stack', 'star', 'steeringwheel', 'stop', 'sub', 'submarine', 'support', 'swatches', 'tablet', 'takeoff', 'target', 'taxi', 'toolbox', 'tools', 'tractor', 'traffic', 'train', 'travelerbag', 'trends', 'tripod', 'trophy', 'truck', 'tv', 'typography', 'ufo', 'umbrella', 'unicycle', 'unlocked', 'upload', 'video', 'videocameraclassic', 'videocameracompact', 'volume', 'water', 'weather', 'windsock', 'windy', 'x', 'zoomin', 'zoomout');
        
        /*asb_et*/
        
		$shortcodes['asb_et'] = array(
			'name'     => __( 'ET service box', 'smasb' ),
			'type'     => 'wrap',
			'group'    => 'box',
            'content' =>  __( "Content of the box.", 'smasb' ),             
			'desc'     => __( 'Service boxes with icons by Elegant themes.', 'smasb' ),
			'note'     => sprintf( __( 'Read more on this shortcode %s here %s', 'sm' ), '<a href="https://lizatom.com/wiki/shortcodes-master-animated-boxes/#shortcodes-master-animated-service-boxes-shortcodes-overview-elegant-themes-service-box" target="_blank">', '&rarr;</a>' ),
			'icon'     => 'columns',
			'function' => array( 'Shortcodes_Master_Asb_Shortcodes', 'asb_et' ),
	            'atts' => array(
                        'id' => array(
                            'type' => 'text',
                            'default' => 'asb-et-unique-id',
                            'name' => __( 'Unique ID', 'smasb' ),
                            'desc' => __( 'This field is required.', 'smasb' )
                        ),
                        'icon' => array(
                            'type' => 'image',
                            'default' => 'image: anchor',
                            'name' => __( 'Icon', 'smasb' ),
                            'values' => $et_icons,
                            'path_small' => plugins_url( 'assets/images/elegant/24px/', SMASB_PLUGIN_FILE ),
                            'path_big' => plugins_url( 'assets/images/elegant/128px/', SMASB_PLUGIN_FILE ),
                            'mime' => 'png',
                            'desc' => __( 'Pick an icon', 'smasb' )
                        ),
                        'animation' => array(
                            'type' => 'select',
                            'values' => array(
                                    'animation-off'   => __( 'animation-off', 'smasb' ),
                                    'bounce'   => __( 'bounce', 'smasb' ),
                                    'flash'   => __( 'flash', 'smasb' ),
                                    'pulse'   => __( 'pulse', 'smasb' ),
                                    'shake'   => __( 'shake', 'smasb' ),
                                    'swing'   => __( 'swing', 'smasb' ),
                                    'tada'   => __( 'tada', 'smasb' ),
                                    'wobble'   => __( 'wobble', 'smasb' ),
                                    'bounceIn'   => __( 'bounceIn', 'smasb' ),
                                    'fadeIn'   => __( 'fadeIn', 'smasb' ),
                                    'flip'   => __( 'flip', 'smasb' ),
                                    'flipInX'   => __( 'flipInX', 'smasb' ),
                                    'flipInY'   => __( 'flipInY', 'smasb' ),
                                    'rotateIn'   => __( 'rotateIn', 'smasb' ),
                                    'rollIn'   => __( 'rollIn', 'smasb' ),
                                    'hinge'   => __( 'hinge', 'smasb' )
                                ),
                            'default' => 'grow',
                            'name' => __( 'Icon animation', 'smasb' ),
                            'desc' => __( 'Animation of the icon when hovered.', 'smasb' )
                        ),
                        'icon_bgcolor' => array(
                                        'type' => 'color',
                                        'values' => array( ),
                                        'default' => '#4F5D73',
                                        'name' => __( 'Icon background color', 'smasb' ), 
                                        'desc' => __( 'Icon background color.', 'smasb' )
                        ),
                        'title' => array(
                            'default' => 'Box title',
                            'name' => __( 'Box title', 'smasb' ),
                            'desc' => __( 'Insert title of the box.', 'smasb' )
                        ),
                        'title_bgcolor' => array(
                                        'type' => 'color',
                                        'values' => array( ),
                                        'default' => '#ffffff',
                                        'name' => __( 'Title background color', 'smasb' ), 
                                        'desc' => __( 'Title background color.', 'smasb' )
                        ),
                        'title_color' => array(
                                        'type' => 'color',
                                        'values' => array( ),
                                        'default' => '#2C3E50',
                                        'name' => __( 'Title color', 'smasb' ), 
                                        'desc' => __( 'Title color.', 'smasb' )
                        ),
                        'title_tag' => array(
                            'type' => 'select',
                            'values' => array(
                                    'div'   => __( 'div', 'smasb' ),
                                    'h3'   => __( 'h3', 'smasb' )
                                ),
                            'default' => 'div',
                            'name' => __( 'Title tag', 'smasb' ),
                            'desc' => __( 'Wrapping tag of the title.', 'smasb' )
                        ),
                        'text_align' => array(
                            'type' => 'select',
                            'values' => array(
                                    'left'   => __( 'left', 'smasb' ),
                                    'right'   => __( 'right', 'smasb' ),
                                    'center'   => __( 'center', 'smasb' ),
                                    'justify'   => __( 'justify', 'smasb' )
                                ),
                            'default' => 'grow',
                            'name' => __( 'Text align', 'smasb' ),
                            'desc' => __( 'Text align.', 'smasb' )
                        ),
                        'text_bgcolor' => array(
                                        'type' => 'color',
                                        'values' => array( ),
                                        'default' => '#ffffff',
                                        'name' => __( 'Background color', 'smasb' ), 
                                        'desc' => __( 'Background color.', 'smasb' )
                        ),
                        'text_color' => array(
                                        'type' => 'color',
                                        'values' => array( ),
                                        'default' => '#2C3E50',
                                        'name' => __( 'Text color', 'smasb' ), 
                                        'desc' => __( 'Text color.', 'smasb' )
                        ),                   
                    )
		);
        
        /*asb_fa*/
        
        $shortcodes['asb_fa'] = array(
            'name'     => __( 'FA service box', 'smasb' ),
            'type'     => 'wrap',
            'group'    => 'box',
            'content' =>  __( "Content of the box.", 'smasb' ),             
            'desc'     => __( 'Service boxes with Font Awesome icons.', 'smasb' ),
            'note'     => sprintf( __( 'Read more on this shortcode %s here %s', 'sm' ), '<a href="https://lizatom.com/wiki/shortcodes-master-animated-boxes/#shortcodes-master-animated-service-boxes-shortcodes-overview-font-awesome-service-box" target="_blank">', '&rarr;</a>' ),
            'icon'     => 'columns',
            'function' => array( 'Shortcodes_Master_Asb_Shortcodes', 'asb_fa' ),
                'atts' => array(
                        'id' => array(
                            'type' => 'text',
                            'default' => 'asb-fa-unique-id',
                            'name' => __( 'Unique ID', 'smasb' ),
                            'desc' => __( 'This field is required.', 'smasb' )
                        ),
                        'icon' => array(
                            'type' => 'icon',
                            'default' => 'icon: star',
                            'name' => __( 'Icon', 'smasb' ),
                            'desc' => __( 'Pick an icon', 'smasb' )
                        ),
                        'animation' => array(
                            'type' => 'select',
                            'values' => array(
                                    'animation-off'   => __( 'animation-off', 'smasb' ),
                                    'bounce'   => __( 'bounce', 'smasb' ),
                                    'flash'   => __( 'flash', 'smasb' ),
                                    'pulse'   => __( 'pulse', 'smasb' ),
                                    'shake'   => __( 'shake', 'smasb' ),
                                    'swing'   => __( 'swing', 'smasb' ),
                                    'tada'   => __( 'tada', 'smasb' ),
                                    'wobble'   => __( 'wobble', 'smasb' ),
                                    'bounceIn'   => __( 'bounceIn', 'smasb' ),
                                    'fadeIn'   => __( 'fadeIn', 'smasb' ),
                                    'flip'   => __( 'flip', 'smasb' ),
                                    'flipInX'   => __( 'flipInX', 'smasb' ),
                                    'flipInY'   => __( 'flipInY', 'smasb' ),
                                    'rotateIn'   => __( 'rotateIn', 'smasb' ),
                                    'rollIn'   => __( 'rollIn', 'smasb' ),
                                    'hinge'   => __( 'hinge', 'smasb' )
                                ),
                            'default' => 'grow',
                            'name' => __( 'Icon animation', 'smasb' ),
                            'desc' => __( 'Animation of the icon when hovered.', 'smasb' )
                        ),
                        'icon_bgcolor' => array(
                                        'type' => 'color',
                                        'values' => array( ),
                                        'default' => '#4F5D73',
                                        'name' => __( 'Icon background color', 'smasb' ), 
                                        'desc' => __( 'Icon background color.', 'smasb' )
                        ),
                        'icon_color' => array(
                                        'type' => 'color',
                                        'values' => array( ),
                                        'default' => '#ffffff',
                                        'name' => __( 'Icon color', 'smasb' ), 
                                        'desc' => __( 'Icon color.', 'smasb' )
                        ),
                        'icon_size' => array(
                                        'type'    => 'slider',
                                        'min'     => 5,
                                        'max'     => 800,
                                        'step'    => 1,
                                        'default' => 30,
                                        'name'    => __( 'Size of the icon', 'smasb' ),
                                        'desc'    => __( 'Size of the icon in pixels (px)', 'smasb' )
                        ),
                        'shadow' => array(
                                        'type'    => 'shadow',
                                        'default' => '1px 1px 2px #AAB2BD',
                                        'name'    => __( 'Shadow', 'sue' ),
                                        'desc'    => __( 'Panel shadow', 'sue' )
                                    ),
                        'title' => array(
                            'default' => 'Box title',
                            'name' => __( 'Box title', 'smasb' ),
                            'desc' => __( 'Insert title of the box.', 'smasb' )
                        ),
                        'title_bgcolor' => array(
                                        'type' => 'color',
                                        'values' => array( ),
                                        'default' => '#ffffff',
                                        'name' => __( 'Title background color', 'smasb' ), 
                                        'desc' => __( 'Title background color.', 'smasb' )
                        ),
                        'title_color' => array(
                                        'type' => 'color',
                                        'values' => array( ),
                                        'default' => '#2C3E50',
                                        'name' => __( 'Title color', 'smasb' ), 
                                        'desc' => __( 'Title color.', 'smasb' )
                        ),
                        'title_tag' => array(
                            'type' => 'select',
                            'values' => array(
                                    'div'   => __( 'div', 'smasb' ),
                                    'h3'   => __( 'h3', 'smasb' )
                                ),
                            'default' => 'div',
                            'name' => __( 'Title tag', 'smasb' ),
                            'desc' => __( 'Wrapping tag of the title.', 'smasb' )
                        ),
                        'text_align' => array(
                            'type' => 'select',
                            'values' => array(
                                    'left'   => __( 'left', 'smasb' ),
                                    'right'   => __( 'right', 'smasb' ),
                                    'center'   => __( 'center', 'smasb' ),
                                    'justify'   => __( 'justify', 'smasb' )
                                ),
                            'default' => 'grow',
                            'name' => __( 'Text align', 'smasb' ),
                            'desc' => __( 'Text align.', 'smasb' )
                        ),
                        'text_bgcolor' => array(
                                        'type' => 'color',
                                        'values' => array( ),
                                        'default' => '#ffffff',
                                        'name' => __( 'Background color', 'smasb' ), 
                                        'desc' => __( 'Background color.', 'smasb' )
                        ),
                        'text_color' => array(
                                        'type' => 'color',
                                        'values' => array( ),
                                        'default' => '#2C3E50',
                                        'name' => __( 'Text color', 'smasb' ), 
                                        'desc' => __( 'Text color.', 'smasb' )
                        ),                   
                    )
        );
        
        /*asb_fc*/
        
        $shortcodes['asb_fc'] = array(
            'name'     => __( 'Flipped card box', 'smasb' ),
            'type'     => 'wrap',
            'group'    => 'box',
            'content' =>  __( "Content of the box.", 'smasb' ),             
            'desc'     => __( 'Flipped card sevice box.', 'smasb' ),
            'note'     => sprintf( __( 'Read more on this shortcode %s here %s', 'sm' ), '<a href="https://lizatom.com/wiki/shortcodes-master-animated-boxes/#shortcodes-master-animated-service-boxes-shortcodes-overview-flipped-card-box" target="_blank">', '&rarr;</a>' ),
            'icon'     => 'columns',
            'function' => array( 'Shortcodes_Master_Asb_Shortcodes', 'asb_fc' ),
                'atts' => array(
                        'class' => array(
                            'type' => 'text',
                            'default' => 'asb-fc-class',
                            'name' => __( 'CSS class', 'smasb' ),
                            'desc' => __( 'CSS class of the box.', 'smasb' )
                        ),
                        'min_height' => array(
                                        'type'    => 'slider',
                                        'min'     => 5,
                                        'max'     => 2000,
                                        'step'    => 1,
                                        'default' => 300,
                                        'name'    => __( 'Minimal height', 'smasb' ),
                                        'desc'    => __( 'Minimal height of the box. (px)', 'smasb' )
                        ),
                    )
        );
                 
		return $shortcodes;
	}
}

new Shortcodes_Master_Asb;
