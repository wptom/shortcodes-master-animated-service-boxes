<?php
class Shortcodes_Master_Asb_Shortcodes
    {
        
    function __construct() { }

/**
 * asb_et
 */
    public static function asb_et($atts = null, $content = null)
        {
        $atts=shortcode_atts(array
            (
            'id' => 'asb-et-unique-id',
            'icon' => 'image: anchor',
            'animation' => 'pulse',
            'icon_bgcolor' => '#4F5D73',
            'title' => 'Box title',            
            'title_bgcolor' => '#ffffff',
            'title_color' => '#2C3E50',
            'title_tag' => 'div',
            'text_align' => 'left',
            'text_bgcolor' => '#ffffff',
            'text_color' => '#2C3E50',
            ), $atts, 'smasb');

        sm_query_asset( 'css', 'font-awesome' );
        sm_query_asset( 'css', 'animate' );
        sm_query_asset( 'css', 'smasb' );
        sm_query_asset( 'css', 'smhover' );
        sm_query_asset( 'js','jquery' );
        sm_query_asset( 'js','smasb' );
        
        // Prepare icon
        if ( $atts['icon'] ) {
            if ( strpos( $atts['icon'], 'image:' ) !== false ) {
                $icon_name = trim( str_replace( 'image:', '', $atts['icon'] ) );
                $icon = '<img src="' . plugins_url( 'assets/images/elegant/128px/', SMASB_PLUGIN_FILE ) . '' . $icon_name . '.png"/>';
            }
            else $icon = '<img src="' . $atts['icon'] . '" alt="' . esc_attr( $content ) . '" />';
        }
        else {  $icon = ''; }
        
        $return = '<div id="' . $atts['id'] . '" class="sm-asb" data-animation="' . $atts['animation'] . '">';
        $return .= '<div class="sm-etsb-icon" style="background-color: ' . $atts['icon_bgcolor'] . ';">'. $icon .'</div>';
        if ( $atts['title'] ) { 
            $return .= '<' . $atts['title_tag'] . ' class="sm-etsb-title" style="background-color: ' . $atts['title_bgcolor'] . '; color: ' . $atts['title_color'] . ';">' . $atts['title'] . '</' . $atts['title_tag'] . '>'; 
        }
        if ( $content ) { 
        $return .= '<div class="sm-etsb-text" style="text-align: ' . $atts['text_align'] . '; background-color: ' . $atts['text_bgcolor'] . '; color: ' . $atts['text_color'] . ';">' .do_shortcode($content). '</div>';
        }
        $return .= '</div>';
            
        return $return;
        }
        
 /**
 * asb_fa
 */
    public static function asb_fa($atts = null, $content = null)
        {
        $atts=shortcode_atts(array
            (
            'id' => 'asb-fa-unique-id',
            'icon' => 'icon: anchor',
            'animation' => 'pulse',
            'icon_bgcolor' => '#4F5D73',
            'icon_color' => '#ffffff',
            'icon_size' => 30,
            'shadow' => '1px 1px 2px #AAB2BD',
            'title' => 'Box title',            
            'title_bgcolor' => '#ffffff',
            'title_color' => '#2C3E50',
            'title_tag' => 'div',
            'text_align' => 'left',
            'text_bgcolor' => '#ffffff',
            'text_color' => '#2C3E50',
            ), $atts, 'smasb');

        sm_query_asset( 'css', 'font-awesome' );
        sm_query_asset( 'css', 'animate' );
        sm_query_asset( 'css', 'smasb' );
        sm_query_asset( 'css', 'smhover' );
        sm_query_asset( 'js','jquery' );
        sm_query_asset( 'js','smasb' );

        //$autoplay = ($atts['autoplay'] === "yes" ? "true" : "false");      
        
        // Prepare icon
        if ( $atts['icon'] ) {
            if ( strpos( $atts['icon'], 'icon:' ) !== false ) {
                $icon = trim( str_replace( 'icon:', '', $atts['icon'] ) );
            }
            else $icon = '<img src="' . $atts['icon'] . '" alt="' . esc_attr( $content ) . '" style="' . implode( $img_css, ';' ) . '" />';
        }
        else {  $icon = ''; $style = 'style="display: block;"'; }
        
        $return = '<div id="' . $atts['id'] . '" class="sm-asb" data-animation="' . $atts['animation'] . '">';
        $return .= '<div class="sm-fasb-icon" style="background-color: ' . $atts['icon_bgcolor'] . ';"><i class="fa fa-' . $icon . '" style="color: ' . $atts['icon_color'] . '; font-size: ' . $atts['icon_size'] . 'px; text-shadow: ' . $atts['shadow'] . '"></i></div>';
        if ( $atts['title'] ) { 
            $return .= '<' . $atts['title_tag'] . ' class="sm-fasb-title" style="background-color: ' . $atts['title_bgcolor'] . '; color: ' . $atts['title_color'] . ';">' . $atts['title'] . '</' . $atts['title_tag'] . '>'; 
        }
        if ( $content ) { 
        $return .= '<div class="sm-fasb-text" style="text-align: ' . $atts['text_align'] . '; background-color: ' . $atts['text_bgcolor'] . '; color: ' . $atts['text_color'] . ';">' .do_shortcode($content). '</div>';
        }
        $return .= '</div>';
            
        return $return;
        }
        
 /**
 * asb_fc
 */
    public static function asb_fc($atts = null, $content = null)
        {
        $atts=shortcode_atts(array
            (
            'class' => 'asb-fc-class',
            'min_height' => 200
            ), $atts, 'smasb');

        sm_query_asset( 'css', 'font-awesome' );
        sm_query_asset( 'css', 'animate' );
        sm_query_asset( 'css', 'smasb' );
        sm_query_asset( 'js','jquery' );
        sm_query_asset( 'js','smasb' );        
        
        $return = '<div class="sm-asb-card-wrapper ' . $atts['asb-fc-class'] . '" style="min-height: ' . $atts['min_height'] . 'px;"><div class="sm-asb-card">';       
        if ( $content ) { 
            $return .= do_shortcode($content);
        }
        $return .= '</div></div>';
            
        return $return;
        }              
        
    }
