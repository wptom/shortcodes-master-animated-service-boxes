<?php
/*
  Plugin Name: Shortcodes Master - Animated service boxes
  Plugin URI: https://lizatom.com/animated-service-box-shortcodes/
  Version: 1.0.0
  Author: Lizatom.com
  Author URI: http://lizatom.com
  Description: Animated service boxes with various icons, colors and effects.
  Text Domain: smasb
  Domain Path: /lang
 */

define( 'SMASB_PLUGIN_FILE', __FILE__ );
define( 'SMASB_PLUGIN_VERSION', '1.0.0' );

require_once 'inc/asb.php';
require_once 'inc/shortcodes.php';
